# 1a
from xml.dom import minidom
# 1b
from xml.sax import parse
from xml.sax.handler import ContentHandler
# 1c
import jellyfish as jf
import numpy as np
# 2a
from sklearn.metrics import precision_recall_fscore_support

def _1a(toPrint=False):
    tvguide = minidom.parse("tvguide-ds.xml")
    if toPrint: print("1a) Length of elements in tvguide-ds.xml: "+str(len(tvguide.getElementsByTagName("movie"))))
    return tvguide

class imdb_Handler(ContentHandler):
    def __init__(self, total_movies=None):
        self.tag = None #Used to store the tag name thats currently being parsed
        self.id = None
        self.string_builder = [] #Used to store the complete title/actor names
        self.string_list = [] #Stores all the movie titles in a list
        self.total_movies = total_movies

    def startElement(self, name, attrs):
        self.tag = name #Initialize tag name
        if name == "movie" and attrs.get('imdbid') is not None : self.id = attrs.values()[0]

    def endElement(self, name):
        self.tag = None #Remove tag name when the tag ends
        if name == "movie" and (self.total_movies is not None and len(self.string_list)>=self.total_movies): #Check if upper limit is reached
            raise Exception("")
        if name == "movie": #If tag is title
            self.string_list.append([" ".join(self.string_builder),self.id]) #append title to list
            self.string_builder = [] #Clear string builder list

    def characters(self, content):
        if content.strip() != "" and self.tag == "title":
            self.string_builder.append(content) #Add to string builder list
    
    # def characters(self, content):
    #     if self.old_tag != self.tag:
    #         self.info_builder.append(self.string_builder)
    #         self.string_builder = []
    #     if content.strip() != "" and self.tag is not None:
    #         self.string_builder.append(content) #Add to string builder list
    #     self.old_tag = self.tag

def _1b(n,toPrint=False):
    handler = imdb_Handler(n) #Pass the number of movies you want to retrieve as a parameter
    try:
        parse("imdb-ds.xml", handler)
    except Exception as e:
        print(e)
        # ass ?
    if toPrint: print("1b) Stopped at handler-limit: "+str(len(handler.string_list))+'\n', handler.string_list)
    return handler.string_list

def _2a(threshold=0.9,toPrint=False):
    tvguide = _1a()
    handler = imdb_Handler() #Pass the number of movies you want to retrieve as a parameter
    matches = []
    misses = []
    tvguide_titles = []
    output = []

    try:
        parse("imdb-ds.xml", handler)
    except Exception as e:
        print(e)

    for t1 in tvguide.getElementsByTagName("movie"):
        tvguide_titles.append(t1.getElementsByTagName('name')[0].firstChild.data)
    for t in tvguide_titles:
        max_title = None
        max_score = -1
        max_id = None
        for i,id in handler.string_list:
            dist = jf.jaro_similarity(t,i)
            if dist > threshold and max_score<threshold:
                max_score = dist
                max_title = i
                max_id = id
        if max_title is not None : 
            matches.append([t,max_title,max_score])
            output.append(max_id)
        else:
            misses.append(t)
            output.append("-")
    # print(matches)
    # print("1de) Found "+str(len(matches))+"/"+str(len(tvguide.getElementsByTagName("movie")))+" comparible titles with certainty of "+str(threshold))
    # print(misses)
    if toPrint: print(output)
    return output

result = _2a(0.8)
relevant = []
with open ("ground_truth.txt","r") as f:
    for line in f.readlines():
        relevant.append(line.strip().split(",")[1])

pre,rec,f1,av = precision_recall_fscore_support(relevant, result, average='micro')
print("Precision: ",pre,"\nRecall:",rec,"\nF1 score:",f1)