import os
# 1a
from xml.dom import minidom
# 1b
from xml.sax import parse
from xml.sax.handler import ContentHandler
# 1c
import jellyfish as jf
import numpy as np

def _1a(toPrint=False):
    tvguide = minidom.parse("tvguide-ds.xml")
    if toPrint: print("1a) Length of elements in tvguide-ds.xml: "+str(len(tvguide.getElementsByTagName("movie"))))
    return tvguide

class imdb_Handler(ContentHandler):
    def __init__(self, total_movies=None):
        self.tag = None #Used to store the tag name thats currently being parsed
        self.string_builder = [] #Used to store the complete title/actor names
        self.string_list = [] #Stores all the movie titles in a list
        self.total_movies = total_movies

    def startElement(self, name, attrs):
        self.tag = name #Initialize tag name

    def endElement(self, name):
        self.tag = None #Remove tag name when the tag ends
        if name == "movie" and (self.total_movies is not None and len(self.string_list)>=self.total_movies): #Check if upper limit is reached
            raise Exception("")
        if name == "movie": #If tag is title
            self.string_list.append(" ".join(self.string_builder)) #append title to list
            self.string_builder = [] #Clear string builder list

    def characters(self, content):
        if content.strip() != "" and self.tag == "title":
            self.string_builder.append(content) #Add to string builder list
    
    # def characters(self, content):
    #     if self.old_tag != self.tag:
    #         self.info_builder.append(self.string_builder)
    #         self.string_builder = []
    #     if content.strip() != "" and self.tag is not None:
    #         self.string_builder.append(content) #Add to string builder list
    #     self.old_tag = self.tag


def _1b(n,toPrint=False):
    handler = imdb_Handler(n) #Pass the number of movies you want to retrieve as a parameter
    try:
        parse("imdb-ds.xml", handler)
    except Exception as e:
        print(e)
        # ass ?
    if toPrint: print("1b) Stopped at handler-limit: "+str(len(handler.string_list))+'\n', handler.string_list)
    return handler.string_list

def _1c(n,dist=0):
    """Get a distance matrix for 'n' titles. Different methods are:
    0: Levenshtein Distance
    1: Jaro-Winkler Distance
    2: Hamming Distance
    3: American Soundex
    """

    titles = _1b(n)
    print("1c) Output below is a matrix with the same indexs as: ")
    print(titles)
    c1 = 0
    c2 = 0
    distances = np.zeros((int(n),int(n)))-1
    for t1 in titles:
        for t2 in titles[c1:]:
            if dist == 0: 
                distances[c1][c2+c1] = jf.levenshtein_distance(t1,t2)
            if dist == 1: 
                distances[c1][c2+c1] = jf.jaro_similarity(t1,t2)
            if dist == 2: 
                distances[c1][c2+c1] = jf.hamming_distance(t1,t2)
            if dist == 3: 
                distances[c1][c2+c1] = jf.soundex(t1,t2)
            if dist > 3:
                raise Exception("Choose a number >4")
            c2+=1
        c1+=1
        c2=0
    print(distances)

def _1de(threshold=0.9):
    tvguide = _1a()
    handler = imdb_Handler() #Pass the number of movies you want to retrieve as a parameter
    matches = []
    misses = []
    tvguide_titles = []

    try:
        parse("imdb-ds.xml", handler)
    except Exception as e:
        print(e)

    for t1 in tvguide.getElementsByTagName("movie"):
        tvguide_titles.append(t1.getElementsByTagName('name')[0].firstChild.data)
    for t in tvguide_titles:
        max_title = None
        max_score = -1
        for i in handler.string_list:
            dist = jf.jaro_similarity(t,i)
            if dist > threshold and max_score<threshold:
                max_score = dist
                max_title = i
        if max_title is not None : 
            matches.append([t,max_title,max_score])
        else:
            misses.append(t)
    print(matches)
    print("1de) Found "+str(len(matches))+"/"+str(len(tvguide.getElementsByTagName("movie")))+" comparible titles with certainty of "+str(threshold))
    print(misses)
    
# _1a(True)
# _1b(15,True)
# _1c(10)
_1de(0.8)
