# 1a
from xml.dom import minidom
# 1b
from xml.sax import parse
from xml.sax.handler import ContentHandler
# 1c
import jellyfish as jf
import numpy as np
# 2a
from sklearn.metrics import precision_recall_fscore_support
from math import sqrt

def _1a(toPrint=False):
    tvguide = minidom.parse("tvguide-ds.xml")
    if toPrint: print("1a) Length of elements in tvguide-ds.xml: "+str(len(tvguide.getElementsByTagName("movie"))))
    return tvguide

def get_tvguide_tiles():
    tvguide_titles = []
    tvguide = minidom.parse("tvguide-ds.xml")
    for t1 in tvguide.getElementsByTagName("movie"):
        tvguide_titles.append(t1.getElementsByTagName('name')[0].firstChild.data)
    return tvguide_titles

def get_tvguide():
    output = []
    tvguide = minidom.parse("tvguide-ds.xml")
    for t1 in tvguide.getElementsByTagName("movie"):
        title = (t1.getElementsByTagName('name')[0].firstChild.data)
        actors = []
        for a in t1.getElementsByTagName('actor'):
            actors.append(a.getElementsByTagName('name')[0].firstChild.data)
        try:
            year = (t1.getElementsByTagName('year')[0].firstChild.data)
        except:
            # Michael Clayton
            # High School Musical 3: Senior Year
            # Pirates Of The Caribbean: The Curse Of The Black Pearl
            # Have no Year
            year = None
        output.append([title,actors,year])
    return output

class imdb_Handler(ContentHandler):
    def __init__(self, total_movies=None):
        self.tag = None #Used to store the tag name thats currently being parsed
        self.id = None
        self.string_builder = [] #Used to store the complete title/actor names
        self.string_list = [] #Stores all the movie titles in a list
        self.total_movies = total_movies

        self.title_builder = []
        self.actor_builder = []
        self.year_builder = [] 

    def startElement(self, name, attrs):
        self.tag = name #Initialize tag name
        if name == "movie" and attrs.get('imdbid') is not None : self.id = attrs.values()[0]

    def endElement(self, name):
        self.tag = None #Remove tag name when the tag ends
        if name == "movie" and (self.total_movies is not None and len(self.string_list)>=self.total_movies): #Check if upper limit is reached
            raise Exception("")
        if name == "movie": #If tag is title
            self.string_list.append([" ".join(self.title_builder),self.id," ".join(self.actor_builder)," ".join(self.year_builder)]) #append title to list
            
            self.title_builder = []
            self.actor_builder = []
            self.year_builder = []


    def characters(self, content):
        if content.strip() != "":
            if self.tag == "title":
                self.title_builder.append(content) #Add to string builder list
            if self.tag == "name":
                self.actor_builder.append(content) 
            if self.tag == "year":
                self.year_builder.append(content) 
                
    # def characters(self, content):
    #     if self.old_tag != self.tag:
    #         self.info_builder.append(self.string_builder)
    #         self.string_builder = []
    #     if content.strip() != "" and self.tag is not None:
    #         self.string_builder.append(content) #Add to string builder list
    #     self.old_tag = self.tag

def _1b(n,toPrint=False):
    handler = imdb_Handler(n) #Pass the number of movies you want to retrieve as a parameter
    try:
        parse("imdb-ds.xml", handler)
    except Exception as e:
        print(e)
        # ass ?
    if toPrint: print("1b) Stopped at handler-limit: "+str(len(handler.string_list))+'\n', handler.string_list)
    return handler.string_list

def _2b(toPrint=False):
    def find_actors_distance(t_actors,i_actors,threshold=0.9):
        scores = []
        count = 0
        for t in t_actors:
            for i in i_actors:
                dist = jf.jaro_similarity(t,i)
                if dist > threshold: 
                    count+=1
                    break
        return sqrt(count/len(t_actors))

    print("Getting tvguide...",end='')
    tvguide = get_tvguide()
    handler = imdb_Handler() #Pass the number of movies you want to retrieve as a parameter
    matches = []
    misses = []
    output = []
    print(" Complete.\nGetting imdb...",end='')
    try:
        parse("imdb-ds.xml", handler)
    except Exception as e:
        print(e)
    print(" Complete.")
    
    count = 0
    length = len(tvguide)
    for t,t_actors,t_year in tvguide:
        count+=1
        max_title = None
        max_score = -1
        max_id = None
        for i,i_id,i_actors,i_year in handler.string_list:
            print(count,"/",length,t,t_year,end=' '*50+'\r')

            dist_title = jf.jaro_similarity(t,i)
            
            try:
                dist_year = (t_year == i_year)
            except:
                # None types and ???? is apperently a thing
                dist_year = 0.2

            if dist_title > 0.5 and dist_year:
                try:
                    dist_actors = find_actors_distance(t_actors,i_actors.split(','))
                except ZeroDivisionError:
                    # No actors in IMDB
                    dist_actors = 0.2
                if max_score<dist_title+dist_year+dist_actors:
                    max_score = dist_title+dist_year+dist_actors
                    max_title = i
                    max_id = i_id
        if max_title is not None : 
            matches.append([t,max_title,max_score])
            output.append(max_id)
        else:
            misses.append(t)
            output.append("-")
    print(matches)
    print("1de) Found "+str(len(matches))+"/"+str(len(tvguide))+" comparible titles")
    print(misses)
    if toPrint: print(output)
    return output
result = _2b()
relevant = []
with open ("ground_truth.txt","r") as f:
    for line in f.readlines():
        relevant.append(line.strip().split(",")[1])

pre,rec,f1,av = precision_recall_fscore_support(relevant, result, average='micro')
print("Precision: ",pre,"\nRecall:",rec,"\nF1 score:",f1)

# dist_year = 0.5; if dist_title > 0.7 and dist_year:
# Found 95/98 comparible titles
# ['The Day The Earth Stood Still', 'Slumdog Millionaire', 'Virtual Jfk: Vietnam If Kennedy Had Lived']
# Precision:  0.6530612244897959 
# Recall: 0.6530612244897959 
# F1 score: 0.6530612244897959

# dist_year = 0; no authors
# 1de) Found 98/98 comparible titles
# []
# Precision:  0.6428571428571429 
# Recall: 0.6428571428571429 
# F1 score: 0.6428571428571429