# 1a
from email import header
from xml.dom import minidom
# 1b
from xml.sax import parse
from xml.sax.handler import ContentHandler
# 1c
import jellyfish as jf
import numpy as np
# 2a
from sklearn.metrics import precision_recall_fscore_support
from math import sqrt
# 3
import csv
# 4
from lxml import etree as ET

'''
Standilzators:
Title:  - Lowercase
        - IMBD: ", A" and ", The" to front
        First comparing the 2 titles to be exactly the same;
            if so make score +4 (otherwise max [0,1), replaces jaro.similiaritys' 1 with 4), steps:
            - Remove symboles and spaces

Year:   - Checks if the years are the same (allows for a difference of 1, as this happends alot)
        IF NaN: 0.5

Auths:  - Does a jaro similiarity for each author is each list and if the output exceeds 90%, it is assumed to be found.
        IF NaN: 0

Accuracy:
    Precision:  0.8469387755102041
    Recall: 0.8469387755102041
    F1 score: 0.8469387755102041

The wrongly classified: titles & years = [OUTPUT, INPUT, GT], distances = [titles,year,authors]
    "['waste of space', 'quantum of solace', 'quantum project']","['2007', '2008', '2000']","[0.7675070028011204, 1, 0.0]","[0.7631610219845514, 0, 0.0]"
        Wrong Title and Year
    "['taken', 'virtual jfk: vietnam if kennedy had lived', 'virtual']","['2008', '2008', '2000']","[0.7073170731707318, 1, 0]","[0.7235772357723578, 0, 0]"
        Wrong Title and Year
    "['legacy', 'eagle eye', 'the eagle']","['2007', '2008', '1915']","[0.7296296296296297, 1, 0.0]","[0.5462962962962963, 0, 0.0]"
        Wrong Title and Year
    "['goodbye lover', 'forever love', 'forever my love']","['1998', '1998', '1962']","[0.7072649572649573, 1, 0.25]","[0.9333333333333332, 0, 0.0]"
        Wrong Title and Year
    "['transformers', 'transporter 3', 'the transporter']","['2007', '2008', '2002']","[0.8141025641025642, 1, 0]","[0.7386169386169387, 0, 0]"
        Wrong Title and Year
    "['stoneheart', 'towelhead', 'swellhead']","['2008', '2008', '1935']","[0.7555555555555555, 1, 0]","[0.8518518518518517, 0, 0.0]"
        Wrong Title and Year
    "['changeling', 'changeling', 'the changeling']","['2007', '2008', '2008']","[4, 1, 0.0]","[0.8047619047619049, 1, 0]"
        ! Wrong Title (this is an unique case though, could catch this)
    "['jack and diane', 'zack and miri make a porno', 'nackt und hei �  auf mykonos']","['2007', '2008', '1979']","[0.7451159951159951, 1, 0.0]","[0.6487586487586489, 0, 0.0]"
        Wrong Title and Year
    "['ca � tica ana', 'miracle at st. anna', 'miracle on main street']","['2007', '2008', '1939']","[0.7477610109189058, 1, 0.0]","[0.7731259968102074, 0, 0.0]"
        Wrong Title and Year
    "['sex and death 101', 'sex and the city', 'six and the city']","['2007', '2008', '2003']","[0.7908496732026143, 1, 0.0]","[0.9583333333333334, 0, 0.0]"
        Wrong Title and Year
    "['crouching tiger, hidden dragon', 'crouching tiger, hidden dragon', 'wo hu cang long']","['2003', '2000', '2000']","[4, 0, 0]","[0.5787878787878787, 1, 0.1796053020267749]"
        Wrong Title and Year (Looks like GT is wrong.)
    "['stolen moments', 'role models', 'the stolen models']","['2008', '2008', '1913']","[0.7662337662337663, 1, 0]","[0.6657754010695187, 0, 0.0]"
        Wrong Title and Year (Looks like GT is wrong.)
    "['telepathy', ""tyler perry's the family that preys"", 'teresa']","['2007', '2008', '2004']","[0.7153439153439153, 1, 0.0]","[0.7238095238095239, 0, 0.0]"
        Wrong Title and Year
    "['limbo lounge', 'slumdog millionaire', 'stump the millionaire']","['2007', '2008', '2006']","[0.7409622541201489, 1, 0.0]","[0.8345864661654135, 0, 0.0]"
        Wrong Title and Year

    Notes:
    Other years are often wrong aswell (either input or IMDB-set) however it is the most likely link (title) and therefore it will still classify correctly, i.e.:
    41 / 98 | milk 2008 = milk 1999 | 4.0 | GT: True
    58 / 98 | tropic thunder 2008 = tropic thunder 2006 | 4 | GT: True
    75 / 98 | w. 2008 = w 1983 | 4.0 | GT: True
    87 / 98 | saw v 2008 = saw iv 2007 | 1.9444444444444446 | GT: True
    (not all listed)
'''   
class datasets:
    ''' 
    Class to load the different datasets 
    '''
    def __init__(self) -> None:
        print("\tGetting tvguide...",end='')
        self.tvguide = self._get_tvguide()
        
        print(" Complete.\n\tGetting imdb...",end='')
        self.handler = imdb_Handler() #Pass the number of movies you want to retrieve as a parameter
        try:
            parse("imdb-ds.xml", self.handler)
        except Exception as e:
            print(e)

        print(" Complete.\n\tGetting groundtruth...",end='')
        self.relevant = []
        with open ("ground_truth.txt","r") as f:
            for line in f.readlines():
                self.relevant.append(line.strip().split(",")[1])
        print(" Complete.")

    ''' Loading the TV guide, but only the relevent data '''
    def _get_tvguide(self):
        output = []
        tvguide = minidom.parse("tvguide-ds.xml")
        for t1 in tvguide.getElementsByTagName("movie"):
            id = t1.getAttribute("tvgid")
            title = (t1.getElementsByTagName('name')[0].firstChild.data)
            actors = []
            for a in t1.getElementsByTagName('actor'):
                actors.append(a.getElementsByTagName('name')[0].firstChild.data)
            try:
                year = (t1.getElementsByTagName('year')[0].firstChild.data)
            except:
                # Michael Clayton
                # High School Musical 3: Senior Year
                # Pirates Of The Caribbean: The Curse Of The Black Pearl
                # Have no Year
                year = None
            output.append([id,title,actors,year])
        return output

    def get_tv_guide(self):
        return self.tvguide
    
    def get_imdb(self):
        return self.handler.string_list
    
    def get_groundtruth(self):
        return self.relevant

class imdb_Handler(ContentHandler):
    def __init__(self, total_movies=None):
        self.tag = None #Used to store the tag name thats currently being parsed
        self.id = None
        self.string_builder = [] #Used to store the complete title/actor names
        self.string_list = [] #Stores all the movie titles in a list
        self.total_movies = total_movies

        # Different Builders for different things.
        self.title_builder = []
        self.actor_builder = []
        self.year_builder = [] 

    def startElement(self, name, attrs):
        self.tag = name #Initialize tag name
        if name == "movie" and attrs.get('imdbid') is not None : self.id = attrs.values()[0]    # Retrieves the IMDB ID of the movie

    def endElement(self, name):
        self.tag = None #Remove tag name when the tag ends
        if name == "movie" and (self.total_movies is not None and len(self.string_list)>=self.total_movies): #Check if upper limit is reached
            raise Exception("")
        if name == "movie": #If tag is title           
            # Fix title for standarization
            title = " ".join(self.title_builder).lower()
            if title[-5:] == ", the": title = "the "+title[:-5]
            if title[-3:] == ", a": title = "a "+title[:-3]
            actors = []
            # Change LAST FIRST to FIRST LAST names
            for actor in self.actor_builder:
                _actor = actor.split(", ")
                actors.append(" ".join(_actor[::-1]))

            self.string_list.append([title,self.id,actors," ".join(self.year_builder)]) #append title to list
            
            # Reset builders
            self.title_builder = []
            self.actor_builder = []
            self.year_builder = []

    def characters(self, content):
        if content.strip() != "":
            # Append to correct tag
            if self.tag == "title":
                self.title_builder.append(content)
            if self.tag == "name":
                self.actor_builder.append(content) 
            if self.tag == "year":
                self.year_builder.append(content)

def _2b(ds,relevant,toPrint=False):
    def find_title_distance(t,i):
        ''' Finds distance between 2 titles with Jaro similarity '''
        # Checks if the title is exactly the same, removing symblos and spaces.
        if ''.join(_t for _t in t if _t.isalnum()) == ''.join(_i for _i in i if _i.isalnum()):
            dist_title = 4
        else:
            dist_title = jf.jaro_similarity(t,i)
        if dist_title < 0.5: dist_title = 0     # set lower limit to remove very unlikely matchs
        return dist_title

    def find_year_distance(t_year,i_year):
        ''' Finds difference between the given years, allows for a +/- 1 difference '''
        try:
            if t_year is None or i_year is None:
                dist_year = 0.5
            else:
                dist_year = int((abs(int(t_year) - int(i_year))<=1))
        except:
            # None types and ???? is apperently a thing
            dist_year = 0.5
        return dist_year

    def find_actors_distance(t_actors,i_actors,threshold=0.9):
        ''' Finds the similarity between the actor lists, cross referencing name by name '''
        if len(t_actors)==0 or len(i_actors)==0: return 0
        count = 0
        for t in t_actors:
            for i in i_actors:
                dist = jf.jaro_similarity(t,i)
                if dist > threshold: 
                    count+=1        # Only save the amount of very likely matches
                    break
        return sqrt(count/len(t_actors))    # SQRT of the ratio of found to have 50% count as ~70% making the requirement less hard
        
    matches = []
    misses = []
    output = []
    wrongs = dict()
    count = 0

    tvguide = ds.get_tv_guide()
    length = len(tvguide)

    ''' Run through every movie in the TV guide and check all movies of IMDB '''
    for id,t,t_actors,t_year in tvguide:
        t = t.lower()
        count+=1
        max_title = None
        max_score = -1
        max_id = None
        max_year = None

        for i,i_id,i_actors,i_year in ds.get_imdb():
            dist_title = find_title_distance(t,i)
            dist_year = find_year_distance(t_year,i_year)

            # Only continue if there is a possible match, so when the title and year have possible match
            if dist_title+int(dist_year)>1.7:
                dist_actors = find_actors_distance(t_actors,i_actors)

                # If the new match has a higher score, save it as the max.
                if max_score<dist_title+dist_year+dist_actors:
                    max_score = dist_title+dist_year+dist_actors
                    max_year = i_year
                    max_title = i
                    max_id = i_id
                    max_score_list = [dist_title,dist_year,dist_actors]
        if max_title is not None : 
            # print(count,"/",length,"|",t,t_year,"=",max_title,max_year,"|",max_score,"| GT:",(relevant[count-1]==max_id))
            print("\tRunning... ",count,"/",length,end='\r')
            if not (relevant[count-1]==max_id): wrongs[max_id]=[max_score_list,relevant[count-1],t,t_year,t_actors]
            matches.append([t,max_title,max_score])
            output.append([id,max_id])
        else:
            # If there is no max_title, there was no match that fits therefore it has been missed
            misses.append(t)
            output.append([id,"-"])

    # Gets the information with the wrong GTs:
    print("\tRunning... Complete. Saving GT mistakes...",end="")
    with open("misc/wrong_results.csv","w",newline='') as csvfile:
        csvwrite = csv.writer(csvfile)
        for i,i_id,i_actors,i_year in ds.get_imdb():
            if i_id in wrongs:
                max_score_list,correct_id,t,t_year,t_actors = wrongs[i_id]
                for _i,_i_id,_i_actors,_i_year in ds.get_imdb():
                    if _i_id == correct_id:
                        correct_score_list = [find_title_distance(t,_i),find_year_distance(t_year,_i_year),find_actors_distance(t_actors,_i_actors)]
                        break
                # csvwrite.writerow([[i,i_id,i_year,i_actors],[_i,_i_id,_i_year,_i_actors],max_score_list,correct_score_list,[max_id,t,t_year,t_actors]])
                csvwrite.writerow([[i,t,_i],[i_year,t_year,_i_year],max_score_list,correct_score_list])
    if toPrint: print(output)
    print(" Complete.")
    print("Complete.")
    return output

class merger_Handler(ContentHandler):
    ''' Runs through like the IMDB handler, but only saves the desired ID's, puts it all in an dictonary to index easily to '''
    def __init__(self,items=None):
        self.tag = None #Used to store the tag name thats currently being parsed
        self.id = None
        self.rdict = items
        self.string_builder = dict()
        self.string_list = dict()
        
    def startElement(self, name, attrs):
        self.tag = name #Initialize tag name
        if name == "movie" and (attrs.get('imdbid') is not None or attrs.get('tvgid') is not None): self.id = attrs.values()[0]

    def endElement(self, name):
        self.tag = None #Remove tag name when the tag ends
        if name == "movie": #If tag is title           
            if len(self.string_builder)>0:
                self.string_list[self.id] = self.string_builder #append title to list
                self.string_builder = dict()

    def characters(self, content):
        if content.strip() != "":
            if self.rdict is None or self.id in self.rdict: # Checks if ID is in the list, otherwise skip.
                tag = self.tag
                if tag in self.string_builder.keys():
                    self.string_builder[tag] = self.string_builder[tag] +"|"+ content 
                else:
                    self.string_builder[tag] = content 

def _4(results):
    def select(tags,a):
        ''' get the info from the merger_Handler in to the Tree '''
        elements = handler_imdb.string_list[id_imdb].keys()
        for i in tags:
            if i in elements:
                ET.SubElement(movie,i).text = ', '.join(handler_imdb.string_list[id_imdb][i].split("|"))

    def combine(tags,A,B):
        ''' Used to combine the list of actors and genre '''
        for t in tags:
            a = ""
            b = ""
            if tags[t] in A: a = A[tags[t]]
            if tags[t] in B: b = B[tags[t]]
            merged = list(set(a.split("|") + b.split("|")))     # LIST -> SET -> LIST filters duplicates
            for i in range(len(merged)-1):
                for j in range(i+1,len(merged)-1):
                    if jf.jaro_similarity(merged[i],merged[j]) > 0.9:
                        merged.pop(j)
            ET.SubElement(movie,t).text = ', '.join(merged)

    results_dict = dict()
    for i,o in results:
        if i != "-" and o != "-":
            results_dict[o] = i
        # imdb : tv

    # Reloads clean datasets to make sure nothing has been changed
    print("\tGetting tvguide...",end='')
    handler_tvguide = merger_Handler()
    parse("tvguide-ds.xml",handler_tvguide)
    print(" Complete.\n\tGetting imdb...",end='')
    handler_imdb = merger_Handler(items=results_dict)
    parse("imdb-ds.xml", handler_imdb)
    print(" Complete.\n\tMerging...",end='')

    # Save the merged database into a Tree.
    root = ET.Element("root")
    doc = ET.SubElement(root,"movies")
    for i in results_dict:
        id_imdb = i
        id_tv = results_dict[i]
        movie = ET.SubElement(doc,"movie", attrib={'imdbid':id_imdb,'tvgid':id_tv})
        select(["title","year","director","location","keyword","plot"],handler_imdb.string_list[id_imdb])
        combine({"genre":"genre","actors":"name"},handler_imdb.string_list[id_imdb],handler_tvguide.string_list[id_tv])
    print(" Complete.")
    tree = ET.ElementTree(root)
    tree.write("merged.xml",pretty_print=True)

################ RUN #################
print("Starting machting process...")
ds = datasets()
relevant = ds.get_groundtruth()
result = _2b(ds,relevant)
# with open('result.csv','w',newline='') as csvfile:
#     writer = csv.writer(csvfile)
#     writer.writerows(result)
################ LOAD #################
# result = []
# with open('result.csv', newline='\n') as csvfile:
#     reader = csv.reader(csvfile)
#     for i in reader:
#         result.append(i)
# relevant = []
# with open ("misc/ground_truth.txt","r") as f:
#     for line in f.readlines():
#         relevant.append(line.strip().split(",")[1])
######################################
_results = []
for i in result: _results.append(i[1])
pre,rec,f1,av = precision_recall_fscore_support(relevant, _results, average='micro')
print("Results of matching:\n","\tPrecision: ",pre,"\n\tRecall:",rec,"\n\tF1 score:",f1)

print("Starting merging process...")
_4(result)
print("Complete.")