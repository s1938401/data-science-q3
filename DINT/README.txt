This is for the course Data Science (2021-2A) - Topic Data Integeration
Students: Alex Mo, Max Lievense

The assignment was to merge the tv-guide-ds.xml entries with the matching imdb-ds.xml.
The final result gave a accuracy of 84.7%, the remaining mistakes are explained in the header of the code.

There are 5 python programs where the name of te final corresponds to the exercise.
This was done to show the step by step implementation of the code.
However, only the last code has been commented completely.

To run the code run the 4_merging.py (you might need to install the packages)
In the code, options are given to skip certain steps, as the output of the steps are saved (to make testing faster, this can be removed.)